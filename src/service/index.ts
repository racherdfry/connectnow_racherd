import axios, { AxiosResponse } from "axios";
import { VideoGame } from "@/types";

export async function getVideoGames(): Promise<VideoGame[]> {
  const axiosConfig = {
    method: "get",
    url: process.env.VUE_APP_API_BASE_PATH + "/applicant-test",
  };

  const response: AxiosResponse = await axios(axiosConfig);
  return response.data;
}
