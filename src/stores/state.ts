import { FilterOption, SortDirection, SortOption, VideoGame } from "@/types";

export const defaultFilter: FilterOption = {
  name: "",
  score: 0,
  sort: {
    direction: SortDirection.Asc,
    option: SortOption.Release_Date,
  },
};

export type State = {
  originalGames: VideoGame[];
  games: VideoGame[];
  loading: boolean;
  filter: FilterOption;
};

export const state: State = {
  loading: false,
  originalGames: [],
  games: [],
  filter: defaultFilter,
};
