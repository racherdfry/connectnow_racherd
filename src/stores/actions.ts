import { ActionContext, ActionTree } from "vuex";
import { getVideoGames } from "@/service";
import { Mutations, MutationType } from "./mutations";
import { State } from "./state";

export enum ActionTypes {
  GetVideoGames = "GET_ITEMS",
}

type ActionAugments = Omit<ActionContext<State, State>, "commit"> & {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
};

export type Actions = {
  [ActionTypes.GetVideoGames](context: ActionAugments): void;
};

export const actions: ActionTree<State, State> & Actions = {
  async [ActionTypes.GetVideoGames]({ commit, state }) {
    if (!state.originalGames.length) {
      commit(MutationType.SetLoading, true);

      getVideoGames()
        .then((response) => {
          commit(MutationType.SetOriginalGames, response);
          commit(MutationType.SetLoading, false);
        })
        .catch(() => {
          commit(MutationType.SetLoading, false);
        });
    } else {
      commit(MutationType.SetOriginalGames, state.originalGames);
    }
  },
};
