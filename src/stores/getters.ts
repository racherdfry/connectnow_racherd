import { GetterTree } from "vuex";
import { State } from "./state";
import { FilterOption, VideoGame } from "@/types";

export type Getters = {
  videoGames(state: State): VideoGame[];
  filterOption(state: State): FilterOption;
  loading(state: State): boolean;
};

export const getters: GetterTree<State, State> & Getters = {
  videoGames(state) {
    return state.games;
  },
  filterOption(state) {
    return state.filter;
  },
  loading(state) {
    return state.loading;
  },
};
