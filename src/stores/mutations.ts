import { MutationTree } from "vuex";
import { State } from "./state";
import { FilterOption, VideoGame } from "@/types";
import filter from "@/utils/filter";

export enum MutationType {
  SetOriginalGames = "SET_ORIGINAL_GAME",
  SetFilterOptions = "SET_FILTER_OPTIONS",
  SetLoading = "SET_LOADING",
}

export type Mutations = {
  [MutationType.SetOriginalGames](state: State, items: VideoGame[]): void;
  [MutationType.SetFilterOptions](
    state: State,
    filterOption: FilterOption
  ): void;
  [MutationType.SetLoading](state: State, status: boolean): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationType.SetOriginalGames](state, items) {
    state.originalGames = items;
    state.games = filter(items, state.filter);
  },
  [MutationType.SetFilterOptions](state, filterOption) {
    state.games = filter(state.originalGames, filterOption);
    state.filter = filterOption;
  },
  [MutationType.SetLoading](state, status) {
    state.loading = status;
  },
};
