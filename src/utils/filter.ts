import { FilterOption, SortDirection, SortOption, VideoGame } from "@/types";

export default function (
  originalGames: VideoGame[],
  filterOption: FilterOption
): VideoGame[] {
  const filtered = originalGames.filter((item) => {
    const isMatchWithName =
      item.name.toLowerCase().indexOf(filterOption.name.toLowerCase()) > -1;
    const isMatchWithScore = item.rating > filterOption.score;

    return isMatchWithName && isMatchWithScore;
  });

  return filtered.sort((a, b) => {
    const { option, direction } = filterOption.sort;

    switch (option) {
      case SortOption.Score: {
        return direction === SortDirection.Asc
          ? a.rating - b.rating
          : b.rating - a.rating;
      }
      case SortOption.Name: {
        if (a.name.toUpperCase() < b.name.toUpperCase()) {
          return direction === SortDirection.Asc ? -1 : 1;
        }
        if (a.name.toUpperCase() > b.name.toUpperCase()) {
          return direction === SortDirection.Asc ? 1 : 1;
        }
        return 0;
      }
      default: {
        return direction === SortDirection.Desc
          ? a.first_release_date - b.first_release_date
          : b.first_release_date - a.first_release_date;
      }
    }
  });
}
