import {
  createRouter,
  createWebHistory,
  Router,
  RouteRecordRaw,
} from "vue-router";

import Home from "@/pages/Home.vue";
import Contact from "@/pages/Contact.vue";

export function setupRouter(): Router {
  const routes: RouteRecordRaw[] = [
    {
      path: "/",
      name: "Home",
      component: Home,
      meta: {},
    },
    {
      path: "/contact",
      name: "Contact",
      component: Contact,
      meta: {},
    },
  ];

  const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
  });

  router.beforeEach((to, from, next) => {
    window.scrollTo(0, 0);

    next();
  });

  return router;
}
