import { createApp } from "vue";
import { store } from "./stores";
import App from "./App.vue";
import { setupRouter } from "@/router";

const router = setupRouter();

const app = createApp(App).use(router).use(store);

app.mount("#app");
