export type VideoGame = {
  id: number;
  first_release_date: number;
  name: string;
  rating: number;
  summary: string;
};

export enum SortOption {
  Release_Date = "release_date",
  Score = "score",
  Name = "name",
}

export enum SortDirection {
  Asc = "asc",
  Desc = "desc",
}

export type FilterOption = {
  name: string;
  score: number;
  sort: {
    direction: SortDirection;
    option: SortOption;
  };
};
